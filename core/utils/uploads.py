import os
import re
import uuid

from io import BytesIO
from PIL import Image

from django.core.files.base import ContentFile

from stdimage.utils import render_variations

#gera a imagem original com no máximo o tamanho informado
from webapp import settings


def preprocess(file_name, variations, storage):
    with storage.open(file_name) as f:
        with Image.open(f) as image:
            file_format = image.format

            # resize to a maximum of 745x745 keeping aspect ratio
            image.thumbnail((745, 745), resample=Image.ANTIALIAS)

            with BytesIO() as file_buffer:
                image.save(file_buffer, file_format)
                f = ContentFile(file_buffer.getvalue())
                # delete the original big image
                storage.delete(file_name)
                # save the resized version with the same filename and format
                storage.save(file_name, f)

    # render stdimage variations
    render_variations(file_name, variations, replace=True, storage=storage)

    return False  # prevent default rendering

def preprocess_logo(file_name, variations, storage):
    with storage.open(file_name) as f:
        with Image.open(f) as image:
            file_format = image.format
            image.thumbnail((600, 600), resample=Image.ANTIALIAS)
            with BytesIO() as file_buffer:
                image.save(file_buffer, file_format)
                f = ContentFile(file_buffer.getvalue())
                storage.delete(file_name)
                storage.save(file_name, f)
    render_variations(file_name, variations, replace=True, storage=storage)
    return False

def preprocess_favicon(file_name, variations, storage):
    with storage.open(file_name) as f:
        with Image.open(f) as image:
            file_format = image.format
            image.thumbnail((32, 32), resample=Image.ANTIALIAS)
            with BytesIO() as file_buffer:
                image.save(file_buffer, file_format)
                f = ContentFile(file_buffer.getvalue())
                storage.delete(file_name)
                storage.save(file_name, f)
    render_variations(file_name, variations, replace=True, storage=storage)
    return False

def preprocess_icon(file_name, variations, storage):
    with storage.open(file_name) as f:
        with Image.open(f) as image:
            file_format = image.format
            image.thumbnail((36, 36), resample=Image.ANTIALIAS)
            with BytesIO() as file_buffer:
                image.save(file_buffer, file_format)
                f = ContentFile(file_buffer.getvalue())
                storage.delete(file_name)
                storage.save(file_name, f)
    render_variations(file_name, variations, replace=True, storage=storage)
    return False

def preprocess_modelo(file_name, variations, storage):
    with storage.open(file_name) as f:
        with Image.open(f) as image:
            file_format = image.format
            image.thumbnail((800, 800), resample=Image.ANTIALIAS)
            with BytesIO() as file_buffer:
                image.save(file_buffer, file_format)
                f = ContentFile(file_buffer.getvalue())
                storage.delete(file_name)
                storage.save(file_name, f)
    render_variations(file_name, variations, replace=True, storage=storage)
    return False

def preprocess_banner_home(file_name, variations, storage):
    with storage.open(file_name) as f:
        with Image.open(f) as image:
            file_format = image.format
            image.thumbnail((877, 252), resample=Image.ANTIALIAS)
            with BytesIO() as file_buffer:
                image.save(file_buffer, file_format)
                f = ContentFile(file_buffer.getvalue())
                storage.delete(file_name)
                storage.save(file_name, f)
    render_variations(file_name, variations, replace=True, storage=storage)
    return False

def letra_numero(d):
    d = re.sub(r'[^a-zA-Z0-9]', ' ', d)
    d = re.sub(r'\s{2,}', ' ', d)
    return d.lower().replace(' ','_')

def empresa_upload_to(instance, filename):
    fpath = 'uploads/empresa/'
    fname, fext = os.path.splitext(filename)
    fname = letra_numero(str(fname))
    return os.path.join(fpath, fname + '_' + str(uuid.uuid4()) + fext)

def banner_empresa_upload_to(instance, filename):
    fpath = 'uploads/empresa/banner-home/'
    fname, fext = os.path.splitext(filename)
    fname = letra_numero(str(fname))
    return os.path.join(fpath, fname + '_' + str(uuid.uuid4()) + fext)

def icone_upload_to(instance, filename):
    fpath = 'uploads/icone/'
    fname, fext = os.path.splitext(filename)
    fname = letra_numero(str(fname))
    return os.path.join(fpath, fname + '_' + str(uuid.uuid4()) + fext)

def modelo_upload_to(instance, filename):
    fpath = 'uploads/produto/'
    fname, fext = os.path.splitext(filename)
    fname = letra_numero(str(instance.nome))
    return os.path.join(fpath, fname + '_' + str(uuid.uuid4()) + fext)

def receita_upload_to(instance, filename):
    fpath = 'uploads/receita/'
    fname, fext = os.path.splitext(filename)
    fname = letra_numero(str(instance.titulo))
    return os.path.join(fpath, fname + '_' + str(uuid.uuid4()) + fext)

def json_upload_to(instance, filename):
    fpath = 'uploads/json-produtos/'
    fname, fext = os.path.splitext(filename)
    return os.path.join(fpath, fname + '_' + str(uuid.uuid4()) + fext)


def upload_s3_9market(dir_image):
    import boto
    import boto.s3.connection
    from boto.s3.key import Key
    conn = boto.s3.connect_to_region(
            'us-east-1',
            aws_access_key_id='AKIAIYARDX25NPLJHFHQ',
            aws_secret_access_key='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',
            calling_format=boto.s3.connection.OrdinaryCallingFormat(),
    )

    bucket = conn.get_bucket('9market')
    full_key_name = os.path.join('media/uploads', dir_image)
    k = bucket.new_key(full_key_name)
    k.set_contents_from_filename(dir_image, policy='public-read')
    return k.generate_url(expires_in=0, query_auth=False)


def upload_s3(dir_image):
    import boto
    import boto.s3.connection
    from boto.s3.key import Key
    conn = boto.s3.connect_to_region(
            'us-east-1',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            calling_format=boto.s3.connection.OrdinaryCallingFormat(),
    )

    bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
    full_key_name = os.path.join('media/uploads', dir_image)
    k = bucket.new_key(full_key_name)
    k.set_contents_from_filename(dir_image, policy='public-read')
    return k.generate_url(expires_in=0, query_auth=False)


def get_url_file_s3(output_path):
    import boto
    import boto.s3.connection
    from boto.s3.key import Key
    conn = boto.s3.connect_to_region(
            'us-east-1',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            calling_format=boto.s3.connection.OrdinaryCallingFormat(),
    )

    bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
    key = bucket.get_key(output_path)
    if key:
        return key.generate_url(expires_in=0, query_auth=False)
    return False