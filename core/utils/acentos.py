
import re
from unicodedata import normalize
from django.template.defaultfilters import slugify


def meu_slugify(texto):
    return slugify(texto).replace('_', '-')


def remover_acentos(txt, codif='iso-8859-1'):

    ''' Devolve cópia de uma str substituindo os caracteres
        acentuados pelos seus equivalentes não acentuados.

        ATENÇÃO: carateres gráficos não ASCII e não alfa-numéricos,
        tais como bullets, travessões, aspas assimétricas, etc.
        são simplesmente removidos!

    '''
    return normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore')




MAIUSC = 'Aa|Aaa|Abnt|Adsl|Agp|Am|Amd|Aoc|Ap|Ati|Atx|Bak|Dimm|Dvi|Dvix|Ecc|Ecs|Eee|Eeepc|Evga|Gpu|Hdmi|Ide|Ii|Iii|Ip|Iv|Led|Lga|Msi|Nec|Ocz|Oem|Pal|Pci|Pcmcia|Pga|Piv|Rim|Sata|Sataii|Scsi|Svga|Ups|Us|Usb|Vga'
CUSTOM = {'(?i)IP(AD|HONE|OD)': lambda m: 'iP'+m.group(1).lower(), 'CORE': 'Core', r'(iii?)\b': lambda m: m.group(0).upper(), r'WII': 'Wii','PLAYSTATION': 'Playstation', 'Pcie(xp)?': 'PCI-Exp'}

def primeira_letra_maiuscula(p):

    # Separa os / para não ficar muito longo sem espaço
    if p.count('/') >=4:
        p = re.sub(r'/([^\s])', r'/ \1', p)

    # Maiuscula se tiver numero
    p = re.sub(r"(\b[\w-]*?\d[\w-]*?\b)", lambda m: m.group(1).upper(), p.title())

    # Maiuscula se nao tiver vogal, os codigos representam áâçéó etc. dos padrões
    p = re.sub(r"(\b[^aeiouAEIOU\xe1\xe2\xe3\xe7\xe9\xea\xed\xf3\xf5\xfa]+?\b)", lambda m: m.group(1).upper(), p)

    # Maiuscula se for alguma palavra da lista
    p = re.sub(r"(\b("+MAIUSC+r")\b)", lambda m: m.group(1).upper(), p)

    # Minuscula se for alguma preposicao/abreviacao
    p = re.sub(r"(\b( C/|D[eo]\b|E\b|I[ -]?[3-9]\b| P/|Para\b| S/F?|WWW|HTTP|Com\b|S\b))", lambda m: m.group(1).lower(), p)

    for k,v in CUSTOM.items():
        p = re.sub(k, v, p)

    return p



"""
_table = {
    "\xe0" : "a", "\xe1" : "a", "\xe2" : "a", "\xe3" : "a",
    "\xe8" : "e", "\xe9" : "e", "\xea" : "e",
    "\xec" : "i", "\xed" : "i", "\xee" : "i",
    "\xf2" : "o", "\xf3" : "o", "\xf4" : "o", "\xf5" : "o",
    "\xf9" : "u", "\xfa" : "u", "\xfb" : "u", "\xfc" : "u",
    "\xe7" : "c", "\xf1" : "n",

    "\xc0" : "A", "\xc1" : "A", "\xc2" : "A", "\xc3" : "A",
    "\xc8" : "E", "\xc9" : "E", "\xca" : "E",
    "\xcc" : "I", "\xcd" : "I", "\xce" : "I",
    "\xd2" : "O", "\xd3" : "O", "\xd4" : "O", "\xd5" : "O",
    "\xd6" : "O", "\xd7" : "X", "\xd8" : "O",
    "\xd9" : "U", "\xda" : "U", "\xdb" : "U", "\xdc" : "U",
    "\xdd" : "Y", "\xde" :  "",  "\xc7" : "C", "\xd1": "N",

    # Caracteres estranhos que são substituídos por vazio

    '\x80' : '', '\x81' : '', '\x82' : '', '\x83' : '', '\x84' : '',
    '\x85' : '', '\x86' : '', '\x87' : '', '\x88' : '', '\x89' : '',
    '\x8a' : '', '\x8b' : '', '\x8c' : '', '\x8d' : '', '\x8e' : '',
    '\x8f' : '', '\x90' : '', '\x91' : '', '\x92' : '', '\x93' : '',
    '\x94' : '', '\x95' : '', '\x96' : '', '\x97' : '', '\x98' : '',
    '\x99' : '', '\x9a' : '', '\x9b' : '', '\x9c' : '', '\x9d' : '',
    '\x9e' : '', '\x9f' : '', '\xa0' : '', '\xa1' : '', '\xa2' : '',
    '\xa3' : '', '\xa4' : '', '\xa5' : '', '\xa6' : '', '\xa7' : '',
    '\xa8' : '', '\xa9' : '', '\xaa' : '', '\xab' : '', '\xac' : '',
    '\xad' : '', '\xae' : '', '\xaf' : '', '\xb0' : '', '\xb1' : '',
    '\xb2' : '', '\xb3' : '', '\xb4' : '', '\xb5' : '', '\xb6' : '',
    '\xb7' : '', '\xb8' : '', '\xb9' : '', '\xba' : '', '\xbb' : '',
    '\xbc' : '', '\xbd' : '', '\xbe' : '', '\xbf' : '',

    '\xf8' : '', '\xc6' : '', '\xef' : '',
}

def asciize(s):
    for original, plain in _table.items():
        s = s.replace(original, plain)
    return s
"""