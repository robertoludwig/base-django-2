from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.core import mail


def send_mail(subject, from_, to, template_name, context):
    body = get_template(template_name).render(context)
    email = EmailMessage(
                subject,
                body,
                from_,
                [to],
                reply_to=[from_],
            )
    email.content_subtype = 'html'
    email.send(fail_silently=False)