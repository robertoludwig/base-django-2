# README #


### Python 3.6.5 ### Instalar [pyenv](https://github.com/pyenv/pyenv)

```
$ pyenv install 3.6.5
$ python --version
```

### Clonar Projeto ###
```
$ git clone https://robertoludwig@bitbucket.org/robertoludwig/base-django-2.git
```

### Virtualenv ###

```
$ mkdir nome-projeto 
$ cd nome-projeto 

$ python3 -m venv .venv
$ source .venv/bin/activate
```

### Instalar requirements ###

``` 
$ pip install -r requirements-dev.txt
```

### Configurar .env ###
``` 
$ cp contrib/env-sample .env
$ python contrib/secret_gen.py
adicionar key gerada ao .env em SECRET_KEY
```